# Not Done My Website

## Project Outline
Not Done My Website is a quick webpage generator for developers who don't have time to build a nice under construction webpage and instead would like to just throw something up very quickly. This provides a tool for generating that and also a framework for making your own templates.

## Running
You need to have an instance of PHP running. Then do this in the web folder (e.g htdocs in XAMPP).
```
git clone git@gitlab.com:bladesimmer/notdonemywebsite.git
```
Then go to localhost/notdonemywebsite (or however you have your server set up).

## Creating Custom Templates
Custom templates should go in to the `/templates/` folder. They should be .html files (PHP works too but adds a bit of complexity and overall maybe not worth it because the page is returned as a .html), with the CSS included. You may use CDNs to deliver content but keep in mind that the only file the client recieves is the `index.html`.

### Keywords for template

| Keyword | Replaced by |
|---|---|
| rep_SURL | URL of the website |
| rep_SNAME | Name of the website |
| rep_DEVNAME | Name of the developer/designer |
| rep_PCOLOUR | Primary colour (hex value)|
| rep_SCOLOUR | Secondary colour (hex value) |